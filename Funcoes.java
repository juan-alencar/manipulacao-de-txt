
import java.io.IOException;																// Importando bibliotecas
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.PrintWriter;

public class Funcoes {

	public static void writeTxt (File filePath, String fileContent) {					// Função para Escrever no arquivo 
		try(	FileWriter create = new FileWriter(filePath, true);						// Cria um novo arquivo no local indicado no argumento filePath
				BufferedWriter buffer = new BufferedWriter(create);						// Cria um buffer para receber o texto do usuário
				PrintWriter writer = new PrintWriter(buffer);)							// Coloca o texto do usuário dentro do "escritor" de arquivo
		{
			writer.append(fileContent);													// Escreve no final do conteúdo do arquivo txt
		}catch(IOException e) {
			e.printStackTrace();
		}		
	}
	
	public static void readTxt(File filePath) {											// Função readTxt serve para ler o arquivo
		try(BufferedReader read = new BufferedReader(new FileReader(filePath))){		// Cria uma variável que ler o buffer do arquivo
			String linha = "";															// Variável que vai conter o conteúdo das linhas do arquivo
			while(true) {																// While para percorrer todo o arquivo
				if(linha!=null) {														// Verifica se existe conteúdo na linha
					System.out.println(linha);											// Se sim, o programa printa ela
				}else {																	// Se não, ele para o While
					break;
				}
				linha = read.readLine();												// Lê a proxima linha
			}			
		}catch(IOException e) {															
			e.printStackTrace();
		}
	}
	
}
