import java.io.File;
import java.util.Scanner;

public class Main {

																					
	public static char menu() {															// Função do para mostrar o menu e extrair escolha do usuário
		String msg;
		System.out.println("-------------------------------");
		System.out.println("----MANIPULANDO ARQUIVO TXT----");
		System.out.println("-------------------------------");
		System.out.println("[1] ESCREVER NO ARQUIVO TXT");	
		System.out.println("[2] MOSTRAR CONTEÚDO DO ARQUIVO");	
		System.out.println("[3] DELETAR O ARQUIVO");
		System.out.println("[4] ENCERRAR PROGRAMA");
		System.out.println("-------------------------------");
		msg = new Scanner(System.in).next();
		return msg.charAt(0);
	}
	
	public static void main(String[] args) {
		
		File filePath = new File("C:\\Users\\manob\\Downloads\\ArquivoProjeto.txt");	// Cria um arquivo .txt nos Downloads
		Scanner buffer = new Scanner(System.in);										// Criando um Buffer para escrever no arquivo
		String content;																	// Variável para colocar o conteúdo do arquivo
		char option;																	// Variável para colocar a opção do menu do usuário
			
		do {																			// DoWhile para o menu rodar pelo menos uma vez
			option = menu();															// Mostra o menu e recolhe a escolha do usuário
			
			switch(option) {															// Switch compara as opções
			
				case '1':																// Opção 1: Escrever no arquivo
					System.out.println("DIGITE O TEXTO ");								// Print intuitivo			
					content = (buffer.nextLine() + "\n");								// Guarda na variável content o conteúdo do buffer	
					Funcoes.writeTxt(filePath, content);								// Chama a função writeTxt que escreve no caminho do arquivo o conteúdo
				break;	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				case '2':																// Opção 2: Ler o arquivo
					if(filePath.exists()) {												// Breve teste para ver se o arquivo existe
						Funcoes.readTxt(filePath);										// Se existe, ele chama a função  readTxt que ler o arquivo
					}else {																// Se não, ele envia uma mensagem de erro e reseta o menu
						System.out.println("ARQUIVO INEXISTENTE");
					}
				break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				case '3':																// Opção 3: Deleta o arquivo
					if(filePath.exists()) {												// Verifica se o arquivo existe
						filePath.delete();												// Se sim, ele apaga o arquivo
						System.out.println("ARQUIVO DELETADO COM SUCESSO");
					}else {																// Se não, ele envia uma mensagem de erro e reseta o menu	
						System.out.println("ARQUIVO INEXISTENTE");
					}
				break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				case '4':																// Opção 4: O programa encerra
					System.out.println("FIM DO PROGRAMA");								// Envia uma mensagem e sai do DoWhile
				break;
//-------------------------------------------------------------------------------------------------------------------------------------------------------------				
				default: 
					System.out.println("OPÇÃO INVÁLIDA");
			}

		}while(option != '4');
	}
}
